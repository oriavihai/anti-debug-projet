#include<stdio.h>
#include<math.h>
#include <time.h>
#include<stdio.h>
#include <windows.h>
#include<stdlib.h>

int antiDebug_var_checksum;
int b = 8;

void antiDebug_destroy_0();
void antiDebug_check_0();
void antiDebug_check_1();
void antiDebug_check_2();
int antiDebug_calculate_checksum();
DWORD WINAPI AntiDebugThread(HANDLE handle);
int foo(int);

int main() {
	unsigned int r,g  = 0;
	CreateThread ( NULL, 0, AntiDebugThread , NULL, 0, NULL );
	srand(time(NULL));  
	r = rand() % 101;
	while(1)
    {
		printf("Enter a guess (0-100): ");
        scanf("%d", &g);
		if (g < r)
			printf("The number is bigger than %d\n", g);
		else if (g > r)
			printf("The number is smaller than %d\n", g);
		else {
			printf("exactly");
			return 0;

		}
    }
	return 0;
}

int foo(int a) {
	return a + b;
}

void antiDebug_destroy_0() {
	OutputDebugString("seriously?\n");
	exit(1);
}

void antiDebug_check_0() {
	OutputDebugString("%s%s%s%s");
}

void antiDebug_check_1() {
	int isDbg = 0;
	char y = 0;

	if (IsDebuggerPresent()) antiDebug_destroy_0();
	CheckRemoteDebuggerPresent(GetCurrentProcess(), &isDbg);
	if (isDbg) antiDebug_destroy_0();
	isDbg = 0;
	_asm {
	push eax
	mov eax, fs:[30h]
	mov al, [eax+68h]
	mov y, al
	pop eax
	}
	if (((y & 0x70)!=0))  antiDebug_destroy_0();	
}

void antiDebug_check_2() {
	if (antiDebug_var_checksum != antiDebug_calculate_checksum())
			antiDebug_destroy_0();
}

int antiDebug_calculate_checksum() {
	int checksum1 = 5381, i = 0, checksum2 = 0, checksum3 = 0, checksum4 = 2166136261;
	void (*antiDebug1Ptr)() = &antiDebug_check_1;
	//int (*calculateChecksumPtr)() = &antiDebug_calculate_checksum;
	void (*antiDebug2Ptr)() = &antiDebug_check_2;
	/*replace_0*/
	char *ptr_0 = (char*)(antiDebug1Ptr);
	//char *ptr_1 = (char*)(calculateChecksumPtr);
	char *ptr_1;
	char *ptr_2 = (char*)(antiDebug2Ptr);
	char *ptr_3 =  NULL;
	
	__asm { 
		push eax
		call $+5
		pop eax
		mov ptr_1, eax
		pop eax
	}
	
	if(ptr_3 == NULL) {
		checksum4 = 0;
	}
	
	for(; i < 0x100; i++) {
		checksum1 = 33 * checksum1 ^ *(ptr_0+i);
		
		checksum2 += *(ptr_1+i);
		checksum2 += (checksum2 << 10);
		checksum2 ^= (checksum2 >> 6);
		
		checksum3 = (*(ptr_2+i)) + (checksum3 << 6) + (checksum3 << 16) - checksum3;
		
		if(ptr_3 != NULL) {
			checksum4 ^= (*(ptr_3+i));
			checksum4 *= 16777619;
		}
	}
	checksum2 += (checksum2 << 3);
	checksum2 ^= (checksum2 >> 11);
	checksum2 += (checksum2 << 15);
	
	ptr_2 = (char*)(antiDebug2Ptr);
	for(i = 0; i < 0x40; i++) {
		if(((*(ptr_2+i)) == 0xCC) || (((*(ptr_2+i)) == 0xCD) && ((*(ptr_2+i+1)) == 0x03))) {
			antiDebug_destroy_0();
		}
		if(((*(ptr_1+i)) == 0xCC) || (((*(ptr_1+i)) == 0xCD) && ((*(ptr_1+i+1)) == 0x03))) {
			antiDebug_destroy_0();
		}
		if(((*(ptr_0+i)) == 0xCC) || (((*(ptr_0+i)) == 0xCD) && ((*(ptr_0+i+1)) == 0x03))) {
			antiDebug_destroy_0();
		}
	}
	return checksum1 ^ checksum2 ^ checksum3 ^ checksum4;
}

DWORD WINAPI AntiDebugThread ( HANDLE handle ) {
	//int isDbg = 0, checksum = antiDebug_calculate_checksum();
	char *p;
	int i, flag = 0;
	
	antiDebug_check_0();

	__asm { 
		push eax
		call $+5
		pop eax
		mov p, eax
		pop eax
	}
	
	p -= 6;
	
	for(i = 0; i < 0xD0; i++) {
		if(((*(p+i)) == 0xCC) || (((*(p+i)) == 0xCD) && ((*(p+i+1)) == 0x03))) {
			antiDebug_destroy_0();
		}
		if((*(p+i)) == 0xFFFFFFE8 || (*(p+i)) == 0xFFFFFFFF || (*(p+i)) == 0xFFFFFF9A0)
			++flag;
	}
	
	if(flag < 5)
		antiDebug_destroy_0();
			
	antiDebug_var_checksum = antiDebug_calculate_checksum();
	
	while(1) {
		antiDebug_check_1();
		antiDebug_check_2();
	}
}