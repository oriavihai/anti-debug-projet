"""
TODO List:
1) change "set_*.add(*)" to file read
2) Auto check of 'preproccesor_len', 'functionSignature_start', 'globalVariable_start', etc...
3) Use in yield?
4) use in header file?
"""

def functionSignatureAdd():
    set_signature = set([])
    set_signature.add("void antiDebug_destroy_0();\n")
    set_signature.add("void antiDebug_check_0();\n")
    set_signature.add("void antiDebug_check_1();\n")
    set_signature.add("void antiDebug_check_2();\n")
    set_signature.add("int antiDebug_calculate_checksum();\n")
    set_signature.add("DWORD WINAPI AntiDebugThread(HANDLE handle);\n")
    return set_signature

def globalVariableAdd():
    set_global = set([])
    set_global.add("int antiDebug_var_checksum;\n")
    return set_global

def functionCallAdd():
    set_call = []
    set_call.append("\tCreateThread ( NULL, 0, AntiDebugThread , NULL, 0, NULL );\n")
    return set_call

def chooseImportantFunction():
    functions = open("function.and", 'r').read() 
    imp_type = "void"
    imp_param = ""
    imp_fun = ""
    c = input("There are important function that you want to protect? (Y/N) ")
    if c == 'Y':
        imp_type = input("Enter the type of return value: ")
        imp_param = input("Enter the type of all parametes (i.e. - int, int, char): ")
        imp_fun = input("Enter the function name: ")
        functions = functions.replace("/*replace_0*/", imp_type + " (*importFunctionPtr)(" + imp_param + ") = &" + imp_fun + ";")
        functions = functions.replace("/*replace_1*/", "char *ptr_3 = (char*)(importFunctionPtr);")
    elif c == 'N':
        functions = functions.replace("/*replace_1*/", "char *ptr_3 =  NULL;")

    return functions

def makeFile():
    file_name = input("Enter a file name, please: ")
    preproccesor_len = int(input("Enter the number of pre-proccesor lines, please: "))
    functionSignature_start = int(input("Enter the start line of function signature, please: "))
    globalVariable_start = int(input("Enter the start line of global variable, please: "))
    main_line = int(input("Enter the number of line of code in main function, please: "))
    
    input_file = open(file_name, "r")
    orginal_code = input_file.readlines()
    code = orginal_code[:preproccesor_len]
    code += open("includes.and", 'r').read()
    code += orginal_code[preproccesor_len:globalVariable_start-1]
    code += open("global.and", 'r').read()
    code += orginal_code[globalVariable_start-1:functionSignature_start-1]
    code += open("signatures.and", 'r').read()
    code += orginal_code[functionSignature_start-1:main_line-1]
    code += open("calls.and", 'r').read()
    code += orginal_code[main_line-1:]
    code += '\n\n'
    code += chooseImportantFunction()
    msg = ''
    for s in code:
        msg += s

    new_file = open("Anti_Debug_"+file_name, 'w')
    new_file.write(msg)
    print("===FINISHED===")
    
makeFile()
