#include<stdio.h>
#include<math.h>
#include <time.h>
#include <windows.h>
#include<stdio.h>
#include<stdlib.h>

int antiDebug_var_checksum;
int b = 8;

void antiDebug_check_0();
DWORD WINAPI AntiDebugThread(HANDLE handle);
void antiDebug_check_2();
int antiDebug_calculate_checksum();
void antiDebug_destroy_0();
void antiDebug_check_1();
int foo(int);

int main() {
	unsigned int r,g  = 0;
	CreateThread ( NULL, 0, AntiDebugThread , NULL, 0, NULL );
	srand(time(NULL));  
	r = rand() % 101;
	while(1)
    {
		printf("Enter a guess (0-100): ");
        scanf("%d", &g);
		if (g < r)
			printf("The number is bigger than %d\n", g);
		else if (g > r)
			printf("The number is smaller than %d\n", g);
		else {
			printf("exactly");
			return 0;

		}
    }
	return 0;
}

int foo(int a) {
	return a + b;
}

void antiDebug_destroy_0() {
	OutputDebugString("seriously?\n");
	exit(1);
}

void antiDebug_check_0() {
	OutputDebugString("%s%s%s%s");
}

void antiDebug_check_1() {
	int isDbg = 0;
	char y = 0;

	if (IsDebuggerPresent()) antiDebug_destroy_0();
	CheckRemoteDebuggerPresent(GetCurrentProcess(), &isDbg);
	if (isDbg) antiDebug_destroy_0();
	isDbg = 0;
	_asm {
	push eax
	mov eax, fs:[30h]
	mov al, [eax+68h]
	mov y, al
	pop eax
	}
	if (((y & 0x70)!=0))  antiDebug_destroy_0();	
}

void antiDebug_check_2() {
	if (antiDebug_var_checksum != antiDebug_calculate_checksum())
			antiDebug_destroy_0();
}

int antiDebug_calculate_checksum() {
	int checksum1 = 5381, i = 0, checksum2 = 0, checksum3 = 0, checksum4 = 2166136261;
	void (*antiDebug1Ptr)() = &antiDebug_check_1;
	int (*calculateChecksumPtr)() = &antiDebug_calculate_checksum;
	void (*antiDebug2Ptr)() = &antiDebug_check_2;
	int (*importFunctionPtr)(int) = &foo;
	char *ptr_0 = (char*)(antiDebug1Ptr);
	char *ptr_1 = (char*)(calculateChecksumPtr);
	char *ptr_2 = (char*)(antiDebug2Ptr);
	char *ptr_3 = (char*)(importFunctionPtr);
	
	if(ptr_3 == NULL) {
		checksum4 = 0;
	}
	
	for(; i < 0x100; i++) {
		checksum1 = 33 * checksum1 ^ *(ptr_0+i);
		
		checksum2 += *(ptr_1+i);
		checksum2 += (checksum2 << 10);
		checksum2 ^= (checksum2 >> 6);
		
		checksum3 = (*(ptr_2+i)) + (checksum3 << 6) + (checksum3 << 16) - checksum3;
		
		if(ptr_3 != NULL) {
			checksum4 ^= (*(ptr_3+i));
			checksum4 *= 16777619;
		}
	}
	checksum2 += (checksum2 << 3);
	checksum2 ^= (checksum2 >> 11);
	checksum2 += (checksum2 << 15);
	
	ptr_2 = (char*)(antiDebug2Ptr);
	for(i = 0; i < 0x40; i++) {
		if(((*(ptr_2+i)) == 0xCC) || (((*(ptr_2+i)) == 0xCD) && ((*(ptr_2+i+1)) == 0x03))) {
			antiDebug_destroy_0();
		}
	}
	return checksum1 ^ checksum2 ^ checksum3 ^ checksum4;
}

DWORD WINAPI AntiDebugThread ( HANDLE handle ) {
	int isDbg = 0, checksum = antiDebug_calculate_checksum();
	char y;
	
	antiDebug_check_0();
	
	antiDebug_var_checksum = antiDebug_calculate_checksum();
	
	while(1) {
		antiDebug_check_1();
		antiDebug_check_2();
	}
}