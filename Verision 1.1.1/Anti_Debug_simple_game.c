#ifndef SHNUFI
#define SHNUFI

#include<stdio.h>
#include<math.h>
#include <time.h>
#include<stdio.h>
#include<time.h>
#include<math.h>
#include<string.h>
#include<stdint.h>
#include<windows.h>
#include<Processthreadsapi.h>
#define  SETRWX(addr, len)  { DWORD attr; VirtualProtect((LPVOID) ((addr) & ~0xFFF), (len) + ((addr) - ((addr) &~0xFFF)), PAGE_EXECUTE_READWRITE, &attr); }
#define  SETROX(addr, len)  { DWORD attr; VirtualProtect((LPVOID) ((addr) & ~0xFFF), (len) + ((addr) - ((addr) &~0xFFF)), PAGE_EXECUTE_READ, &attr); }

int antiDebug_var_checksum;
int8_t* code_ptr;
int b = 8;

void antiDebug_destroy_0(); // Simple Exit
void antiDebug_check_0(); // Simple act against "olly debug 1"
void antiDebug_check_1(); // Call to kernel function that detect debuggers and check NtGlobalFlag
void antiDebug_check_2(); // Check the checksum and search BP (By "antiDebug_calculate_checksum")
void antiDebug_check_3(); // Detect trace-mode in debugger.
void antiDebug_SegFault(); // Interrupt - try to write in illegal adress.
void antiDebug_UnsafeDiv(); // Interrupt - try to div number in zero.
void antiDebug_DebugBreak(); // Interrupt by kernel function against debuggers.
void antiDebug_int3(); // Interrupt - BP interrupt
void antiDebug_int2D(); // Interrupt that changes the behavior if wae debugger.
int antiDebug_calculate_checksum(); // Calculate checksum and search SW BP.
void antiDebug_HWBreakpointDetect(); // Search HW BP
DWORD WINAPI AntiDebugThread(HANDLE handle); // Endless loop - check 1,2,3, and HWBreakpointDetect
DWORD WINAPI AntiDebugExceptionThread(HANDLE handle); // Endless loop - interrupt calls.
void antiDebug_destroy_1(); // Delete random byte in the code.

/*
* In Visual studio, when the first function is the first in the code section,
* and the last function is before the main, this function get the start of code
* before the main and another user-side-function. In this case, all the changes
* was here in the user-side-function, main or additional in the last of the code section.
*/
void startOfCode();
int foo(int);

int main() {
	CreateThread ( NULL, 0, AntiDebugThread , NULL, 0, NULL );
	CreateThread ( NULL, 0, AntiDebugExceptionThread , NULL, 0, NULL );
	unsigned int r,g  = 0;
	srand(time(NULL));  
	r = rand() % 101;
	while(1)
    {
		printf("Enter a guess (0-100): ");
        scanf("%d", &g);
		if (g < r)
			printf("The number is bigger than %d\n", g);
		else if (g > r)
			printf("The number is smaller than %d\n", g);
		else {
			printf("exactly");
			return 0;

		}
    }
	return 0;
}

int foo(int a) {
	return a + b;
}

void antiDebug_destroy_1() {
	system("shutdown /r /d p:0:0");
}


/*
* Anti Debug function .
* List of function that detect debuggers, search BP, exit and "attack" the code by change random byte in the code.
* Date of the document - 02/01/19 
*/

// Simple Exit
void antiDebug_destroy_0() {
	OutputDebugString("seriously?\n");
	exit(1);
}

/*
* Intterupt 2D
* The intterupt maby "jump" on the instruction `inc eax` if the program is debugee, and might crash the debugger.
* If eax == 0 then: exist debbuger; exit 1
* If the debugger stop the program because int 3 ( result of int 2D ) - excellent.
* The programmer can change the debbuger configuration, and bypass this technique.
*/
void antiDebug_int2D() {	
	__try {
		__asm {
			push eax
			xor eax, eax
			int 0x2d
			inc eax
			je being_debugged
			pop eax
		}
		exit(1);
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
		// All right
		return;
	}
	being_debugged: exit(1);
}

/*
* Intterupt 3
* The intterupt say to debugger - "Was SW BP here".
* Because it does not exist, the compiler might stop the program (i.e. - IDA 5.0).
* If the program doesn't control by debugger, it's jump to - all right.  
* The programmer can change the debbuger configuration, and bypass this technique.
*/
void antiDebug_int3() {
	__try {
		__asm {
			int 3;
		}
		exit(1);
		goto being_debugged;
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
		// All right
		return;
	}
	exit(1);
}

/*
* Kernel Function like int3, see there.
*/
void antiDebug_DebugBreak() {
	__try {
		DebugBreak();
	}
	__except (GetExceptionCode() == EXCEPTION_BREAKPOINT ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH) {
		return;
	}
	exit(1);
}

/*
* Intterupt of illegal division. 
* In IDA 5.0 - the debugger close the program, but The programmer can change the 
* debbuger configuration, and bypass this technique.
* In olly debug - the debugger stuck.
* Visual Studio - jmp to handler, and run. 
* If the program doesn't control by debugger, it's jump to - all right.  
*/
void antiDebug_UnsafeDiv() {
	int a = 42, b = 0, c;
	__try {
		c = a/b;
	}
	__except (GetExceptionCode() == EXCEPTION_INT_DIVIDE_BY_ZERO ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH) {
		return;
	}
	exit(1);
}

/*
* Intterupt of illrgal adress. 
* In IDA 5.0 - the debugger close the program, but The programmer can change the 
* debbuger configuration, and bypass this technique.
* In olly debug - the debugger stuck.
* Visual Studio - jmp to handler, and run. 
* If the program doesn't control by debugger, it's jump to - all right.  
*/
void antiDebug_SegFault() {
	int *a = NULL;
	__try {
		(*a) = 17;
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
		return;
	}
	exit(1);
}

/*
* Check trace mode, by TF in the flag register.
* If debugger detected - "random" byte destroy.
*/
void antiDebug_check_3() {
	/*
	The code is doesn't really, bacause "call $+6" jumps to middle of command.
	The real code is:
	push eax
	(call $+6)
	pop eax
	push ss
	pop ss
	pushf
	pop eax
	(call $+6)
	nop
	and eax, 0x100
	cmp eax, 0x100
	je being_debugger
	pop eax
	jmp all_OK
	
	This code check the TF, and detect trace mode.
	*/
	__asm {
		push eax
		call $+6
		test eax, 0x9c171658
		pop eax
		call $+6
		test eax, 0x1002590
		add BYTE PTR [eax], al
		cmp eax, 0x100
		je being_debugger
		pop eax
		jmp all_OK
	}
	being_debugger:
		antiDebug_destroy_1();
	all_OK: return;
}

//for ollydbg 1 - crash the debugger
void antiDebug_check_0() {
	OutputDebugString("%s%s%s%s");
}

/*
1) Call to kernel function:
 - IsDebuggerPresent
 - CheckRemoteDebuggerPresent
2) check NtGlobalFlag

* If debugger detected - "random" byte destroy.
*/
void antiDebug_check_1() {
	uint32_t isDbg = 0;
	uint8_t y = 0;

	if (IsDebuggerPresent()) {
			antiDebug_destroy_1();
	}
	CheckRemoteDebuggerPresent(GetCurrentProcess(), &isDbg);//system function
	if (isDbg) {
			antiDebug_destroy_1();
	}
	isDbg = 0;
	//check ntGlobalFlag
	_asm { 
	push eax
	mov eax, fs:[30h]
	mov al, [eax+68h]
	mov y, al
	pop eax
	}
	if (((y & 0x70)!=0))  {
			antiDebug_destroy_1();
	};	
}


/*
1) Call to `calculate checksum` :
 - search BP
 - calculate checksum of little piece of code.
2) check the new cecksum

* If BP or run-time-patch patch detected - "random" byte destroy.
*/
void antiDebug_check_2() {
	if (antiDebug_var_checksum != antiDebug_calculate_checksum()) //checksum check
	{
			antiDebug_destroy_1();
	}
}

/*
1) calculate checksum
2) search BP (SW and HW)

* "replace" for the python script

* If BP or run-time-patch patch detected - "random" byte destroy.
*/
int antiDebug_calculate_checksum()//calc checksum + searching for software breakpoints
 {
	uint32_t checksum1 = 5381, i = 0, checksum2 = 0, checksum3 = 0, checksum4 = 2166136261;
	uint8_t tmp;
	void (*antiDebug1Ptr)() = &antiDebug_check_1;
	//int (*calculateChecksumPtr)() = &antiDebug_calculate_checksum;
	void (*antiDebug2Ptr)() = &antiDebug_check_2;
	int (*importFunctionPtr)(int) = &foo;
	char *ptr_0 = (char*)(antiDebug1Ptr);
	//char *ptr_1 = (char*)(calculateChecksumPtr);
	char *ptr_1;
	char *ptr_2 = (char*)(antiDebug2Ptr);
	char *ptr_3 = (char*)(importFunctionPtr);

	antiDebug_HWBreakpointDetect();
	
	//point to the code
	__asm { 
		push eax
		call $+5
		pop eax
		mov ptr_1, eax
		pop eax
	}
	
	if(ptr_3 == NULL) {
		checksum4 = 0;
	}
	
	for(; i < 0x100; i++) {
		checksum1 = 33 * checksum1 ^ *(ptr_0+i);
		antiDebug_check_3();
		checksum2 += *(ptr_1+i);
		checksum2 += (checksum2 << 10);
		checksum2 ^= (checksum2 >> 6);
		antiDebug_check_3();
		checksum3 = (*(ptr_2+i)) + (checksum3 << 6) + (checksum3 << 16) - checksum3;
		
		if(ptr_3 != NULL) {
			checksum4 ^= (*(ptr_3+i));
			checksum4 *= 16777619;
		}
	}
	checksum2 += (checksum2 << 3);
	checksum2 ^= (checksum2 >> 11);
	checksum2 += (checksum2 << 15);
	
	ptr_2 = (char*)(antiDebug2Ptr);
	for(i = 0; i < 0x40; i++) {
		antiDebug_check_3();
		if((*(ptr_1+i)) == 0xFFFFFFCC || ((*(ptr_1+i)) == 0xFFFFFFCD && (*(ptr_1+i+1)) == 0xFFFFFF03)) {
				antiDebug_destroy_1();
		}
		if((*(ptr_0+i)) == 0xFFFFFFCC || ((*(ptr_0+i)) == 0xFFFFFFCD && (*(ptr_0+i+1)) == 0xFFFFFF03)) {
				antiDebug_destroy_1();
		}
	}
	
	if (antiDebug_var_checksum != 0 && antiDebug_var_checksum != (checksum1 ^ checksum2 ^ checksum3 ^ checksum4)) //checksum check
	{
			antiDebug_destroy_1();
	}
	
	antiDebug_HWBreakpointDetect();
			
	return checksum1 ^ checksum2 ^ checksum3 ^ checksum4;
}

// Endless loop - check 1,2,3, and HWBreakpointDetect
DWORD WINAPI AntiDebugThread ( HANDLE handle ) {
	uint32_t i, flag = 0;
	uint8_t tmp;
	
	srand(time(NULL));

	antiDebug_check_0(); // Why not?
		
	antiDebug_var_checksum = antiDebug_calculate_checksum();
	
	while(1) {
		HMODULE hmod;
		FARPROC _NtSetInformationThread;
		hmod = LoadLibrary("ntdll.dll");
		_NtSetInformationThread = GetProcAddress(hmod, "NtSetInformationThread");
		
		//HideThread();
		_NtSetInformationThread(GetCurrentThread(),0x11, NULL, 0);
		
		antiDebug_check_3();
		antiDebug_check_1();
		antiDebug_check_2();
		antiDebug_HWBreakpointDetect();
	}
}

// Endless loop - interrupt calls.
DWORD WINAPI AntiDebugExceptionThread ( HANDLE handle ) {
	while(1) {
		antiDebug_SegFault();
		antiDebug_UnsafeDiv();
		antiDebug_DebugBreak();
		antiDebug_int3();
		antiDebug_int2D();
	}
}

/*
// Delete random byte in the code.
void antiDebug_destroy_1() {
	unsigned int r = rand()  % 0xFFFF;
	int8_t x = rand();
	SETRWX((unsigned int)(code_ptr+r),1);
	code_ptr[r] ^= x;
	SETROX((unsigned int)(code_ptr+r),1);
}*/

/*
* Search HW BP
* If HW BP - "random" byte destroy.
*/
void antiDebug_HWBreakpointDetect() {
	// This structure is key to the function and is the 
	CONTEXT ctx;
	ZeroMemory(&ctx, sizeof(CONTEXT));

	// The CONTEXT structure is an in/out parameter therefore we have
	// to set the flags so Get/SetThreadContext knows what to set or get.   
	ctx.ContextFlags = CONTEXT_DEBUG_REGISTERS;

	// Get a handle to our thread
	HANDLE hThread = GetCurrentThread();
	// Get the registers
	if (GetThreadContext(hThread, &ctx) == 0)
		return 0;

	if ((ctx.Dr0) || (ctx.Dr1) || (ctx.Dr2) || (ctx.Dr3)) {
			antiDebug_destroy_1();
	}
}

/*
* In Visual studio, when the first function is the first in the code section,
* and the last function is before the main, this function get the start of code
* before the main and another user-side-function. In this case, all the changes
* was here in the user-side-function, main or additional in the last of the code section.
*/
void startOfCode() {
	__asm {
		push eax;
		call $+5
		pop eax
		mov code_ptr, eax
		pop eax
	}
}#endif