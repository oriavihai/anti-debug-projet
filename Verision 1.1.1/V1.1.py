import os

def chooseImportantFunction():
    functions = open("function.and", 'r').read() 
    imp_type = "void"
    imp_param = ""
    imp_fun = ""
    c = input("There are important function that you want to protect? (Y/N) ")
    if c.upper() == 'Y':
        imp_type = input("Enter the type of return value: ")
        imp_param = input("Enter the type of all parametes (i.e. - int, int, char): ")
        imp_fun = input("Enter the function name: ")
        functions = functions.replace("/*replace_0*/", imp_type + " (*importFunctionPtr)(" + imp_param + ") = &" + imp_fun + ";")
        functions = functions.replace("/*replace_1*/", "char *ptr_3 = (char*)(importFunctionPtr);")
    elif c == 'N':
        functions = functions.replace("/*replace_1*/", "charptr_3 =  NULL;")
        
    return functions

def chooseDestroy():
    print("Which destroy function do you wan't to add if detect debugger?")
    print("0) Simple exit (deafult)\n1) Delete random byte\n2) Shutdown by cmd")

    funsArray = ["destroy\\simple_exit.txt",
                 "destroy\\delete_byte.txt",
                 "destroy\\shut_down.txt"]

    count = 3
    
    more_options_array = []
    paths = os.listdir("destroy\\more_options")
    for p in paths:
        funsArray.append("destroy\\more_options\\" + p)
        print(str(count) + ") " + p)
        
    ch = input("Enter your choise: ")
    f = 0
    
    try:
        f = int(ch)
    except ValueError:
        print("Error")

    file = open(funsArray[f], 'r')
    ret = file.read()
    file.close()

    return ret
    
def makeFile():
    file_name = input("Enter a file name, please: ")
    input_file = open(file_name, "r")
    
    preproccesor_len = int(input("Enter the number of pre-proccesor lines in the start of file, please: "))
    functionSignature_start = int(input("Enter the start line of function signature, please: "))
    globalVariable_start = int(input("Enter the start line of global variable, please: "))
    main_line = int(input("Enter the number of line of code in main function, please: "))
    end_code = int(input("Enter the number of line of the code end, before pre-proccesor interuction like #endif: "))
    orginal_code = input_file.readlines()
    code = orginal_code[:preproccesor_len]
    code += open("includes.and", 'r').read()
    code += orginal_code[preproccesor_len:globalVariable_start-1]
    code += open("global.and", 'r').read()
    code += orginal_code[globalVariable_start-1:functionSignature_start-1]
    code += open("signatures.and", 'r').read()
    code += orginal_code[functionSignature_start-1:main_line-1]
    code += open("calls.and", 'r').read()
    code += orginal_code[main_line-1:end_code]
    code += chooseDestroy()
    code += '\n\n'
    code += chooseImportantFunction()
    code += orginal_code[end_code:]
    msg = ''
    for s in code:
        msg += s

    new_file = open("Anti_Debug_"+file_name, 'w')
    new_file.write(msg)
    print("===FINISHED===")

if __name__== "__main__":
    makeFile()
