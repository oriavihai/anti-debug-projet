"""
TODO List:
1) change "set_*.add(*)" to file read
2) Auto check of 'preproccesor_len', 'functionSignature_start', 'globalVariable_start', etc...
3) Use in yield?
4) use in header file?
"""

def includeAdd():
    set_preproccesor = set([])
    set_preproccesor.add("#include<stdio.h>\n")
    set_preproccesor.add("#include <windows.h>\n")
    set_preproccesor.add("#include<stdio.h>\n")
    set_preproccesor.add("#include <psapi.h>\n")
    return set_preproccesor

def functionSignatureAdd():
    set_signature = set([])
    set_signature.add("void antiDebug0();\n")
    set_signature.add("void antiDebug1();\n")
    set_signature.add("void antiDebug2();\n")
    set_signature.add("void calc();\n")
    set_signature.add("int search();\n")
    return set_signature

def globalVariableAdd():
    set_global = set([])
    set_global.add("int checksum = 5381;\n")
    return set_global

def functionCallAdd():
    set_call = []
    set_call.append("\tcalc();\n")
    set_call.append("\tantiDebug0();\n")
    set_call.append("\tantiDebug1();\n")
    set_call.append("\tantiDebug2();\n")
    return set_call

def makeFile():
    file_name = input("Enter a file name, please: ")
    preproccesor_len = int(input("Enter the number of pre-proccesor lines, please: "))
    functionSignature_start = int(input("Enter the start line of function signature, please: "))
    globalVariable_start = int(input("Enter the start line of global variable, please: "))
    main_line = int(input("Enter the number of line of main function, please: "))
    
    input_file = open(file_name, "r")
    orginal_code = input_file.readlines()
    code = orginal_code[:preproccesor_len]
    for s in includeAdd():
        code += s
    code += orginal_code[preproccesor_len:globalVariable_start-1]
    for s in globalVariableAdd():
        code += s
    code += orginal_code[globalVariable_start-1:functionSignature_start-1]
    for s in functionSignatureAdd():
        code += s
    code += orginal_code[functionSignature_start-1:main_line]
    for s in functionCallAdd():
        code += s
    code += orginal_code[main_line:]
    code += '\n\n'
    code += open("function.fun", 'r').read()
    msg = ''
    for s in code:
        msg += s
    ##print(msg)
    new_file = open("Anti_Debug_"+file_name, 'w')
    new_file.write(msg)
    print("===FINISHED===")
    
makeFile()
