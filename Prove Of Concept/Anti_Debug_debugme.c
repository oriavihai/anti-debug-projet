#include<stdio.h>
#include<math.h>
#include <windows.h>
#include<stdio.h>
#include <psapi.h>

int checksum = 5381;
int a;
int b;
int c;

void antiDebug2();
void calc();
int search();
void antiDebug0();
void antiDebug1();
void foo();
int foo2();

int main() {
	calc();
	antiDebug0();
	antiDebug1();
	antiDebug2();
	printf("Debug me if you can...\n");
	foo();
	foo2();
}

void foo() {
	printf("Foo\n");
}

int foo2() {
	printf("Foo2\n");
	return 0;
}

void antiDebug0() {
	OutputDebugString("%s%s%s%s");
}

void antiDebug1() {
	int isDbg  = 0, x = 0;
	char y;
	
	printf("IsDebuggerPresent - %d\n",IsDebuggerPresent());
	CheckRemoteDebuggerPresent(GetCurrentProcess(), &isDbg);
	printf("CheckRemoteDebuggerPresent - %d\n", isDbg);
	
	_asm {
		push eax
		push ss
		pop ss
		pushfd 
		pop eax
		mov x,  eax
		pop eax
	}
	printf("Trap Flag - %d\n", ((x & 0x100)!=0));	
	
	_asm {
		push eax
		mov eax, fs:[30h]
		mov al, [eax+68h]
		;and al, 70h
		mov y, al
		pop eax
	}
	
	printf("Heap Flag - %d\n\n", ((y & 0x70)!=0));	
	
	if (1 == isDbg) {
		printf("Ave Programmer! Morituri te salutant!\n");
	}
	if ((0x100 == (x & 0x100))) {
		printf("Et tu, Brute?\n");
	}
	if (0x70 == (y & 0x70)) {
		printf("Carthago delenda est.\n");
	}
}

void antiDebug2() {
	int tmp = checksum;
	
	checksum = 5381;
	calc();
	
	if (tmp != checksum || search())
		printf("There are breakpoint or changes in \'antiDebug1\'");
	
}

void calc() {
	void (*antiDebug1Ptr)() = &antiDebug1;
	char *ptr = (char*)(antiDebug1Ptr);
	int i = 0;
	
	for (; i <250; i++) {
		checksum = 33 * checksum ^ *(ptr+i);
	}
}

int search() {
	void (*antiDebug1Ptr)() = &antiDebug1;
	char *ptr = (char*)(antiDebug1Ptr);
	int i = 0;
	for (; i <250; i++) {
		if (*(ptr+i) == 0xCC)
			return 1;
	}
	return 0;
}