import random

functionArray = []

def init():
    pathsArray = ["functions\\check1.txt",
                  "functions\\check3.txt",
                  "functions\\int2d.txt",
                  "functions\\int3.txt",
                  "functions\\int41.txt",
                  "functions\\segFault.txt",
                  "functions\\unsafeDiv.txt"]
    
    global functionArray

    more_option_array = []
    more_paths = os.listdir("functions\\more_options")
    for p in more_paths:
        pathsArray.append("destroy\\more_options\\" + p)
    
    for p in pathsArray:
        f = open(p,'r')
        functionArray.append(f.read())
        f.close()
    
def edit(name):
    msg = ""
    code = ""
    
    in_file = open(name,"r")
    out_file = open("Anti_Debug_" + name, "w")
    init()
        
    i = 0
    flag = ""
    count = 0
    buf = ""
    j = 0
    lines = in_file.readlines()
    while j < len(lines):
        code = lines[j]
        i = 0
        while i < len(code):
            msg += code[i]
            if code[i] == "{":
                if flag == "":
                    if count == 0:
                        if code.strip()[0] == '{':
                            print("You wan't add random anti-debug function to start of `" + buf + "`? (Y is Yes, another letter is No)")
                        else:
                            print("You wan't add random anti-debug function to start of `" + code + "`? (Y is Yes, another letter is No)")   
                        if input().upper() == "Y":
                            msg += "\n" + random.choice(functionArray) + "\n"
                    count += 1
            elif code[i] == "}" and flag == "":
                count -= 1
            elif code[i] == '"' and flag == '"':
                flag = ""
            elif code[i] == "'" and flag == "'":
                flag = ""
            elif (code[i] == "'" or code [i] == '"') and flag == "":
                flag = code[i]
            elif flag != "" and code[i] == "\\":
                msg += code[i+1]
                i += 1
            i += 1
        j += 1
        buf = code[:-1] # without '\n'
    out_file.write(msg)
    print("===FINISH===")

if __name__== "__main__":
    name = input("Enter a name of file: ")
    edit(name)
