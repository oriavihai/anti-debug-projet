#include<windows.h>


void IsHWBreakpointExists()
{
	// This structure is key to the function and is the 
	CONTEXT ctx;
	ZeroMemory(&ctx, sizeof(CONTEXT));

	// The CONTEXT structure is an in/out parameter therefore we have
	// to set the flags so Get/SetThreadContext knows what to set or get.   
	ctx.ContextFlags = CONTEXT_DEBUG_REGISTERS;

	// Get a handle to our thread
	HANDLE hThread = GetCurrentThread();
	// Get the registers
	if (GetThreadContext(hThread, &ctx) == 0)
		return 0;

	if ((ctx.Dr0) || (ctx.Dr1) || (ctx.Dr2) || (ctx.Dr3)) {
		return 1;
	}
	else {
		return 0;
	}
}